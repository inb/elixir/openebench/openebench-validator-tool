/**
 * *****************************************************************************
 * Copyright (C) 2019 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.validator;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import es.elixir.bsc.json.schema.JsonSchemaException;
import es.elixir.bsc.json.schema.JsonSchemaLocator;
import es.elixir.bsc.json.schema.JsonSchemaReader;
import es.elixir.bsc.json.schema.ValidationError;
import es.elixir.bsc.json.schema.model.JsonObjectSchema;
import es.elixir.bsc.json.schema.model.JsonSchema;
import es.elixir.bsc.json.schema.model.JsonStringSchema;
import es.elixir.bsc.json.schema.model.PrimitiveSchema;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import org.bson.Document;

/**
 * @author Dmitry Repchevsky
 */

public class FKValidator {
    
    private final static String HELP = "validator [-host, -port, -u, -p] -database [-provisional]\n\n" +
                                       "parameters:\n\n" +
                                       "-host              - mongodb host (default localhost) \n" +
                                       "-port              - mongodb port (default 27017) \n" +
                                       "-u                 - mongodb user\n" +
                                       "-p                 - mongodb password\n" +
                                       "-database          - mongodb database to validate\n" +
                                       "-provisional       - mongodb additional (provisional) database to validate\n";
    
    private final MongoClient client;
    private final String database;
    
    private final JsonSchemaReader reader;
    private final Map<String, JsonSchemaLocator> locators;
    
    public FKValidator(final MongoClient client, 
                       final String database) {
        
        this.client = client;
        this.database = database;
        
        reader = JsonSchemaReader.getReader();
        locators = new ConcurrentHashMap<>();
    }
    
    /**
     * Validates Foreign Keys in the database.
     * 
     * @param database name of the database to test FKs
     * @param errors list to collect validation errors
     */
    public void validate(String database, List<ValidationError> errors) {
        MongoDatabase db = client.getDatabase(this.database);
        MongoDatabase test = this.database.equals(database) ? db : client.getDatabase(database);
        
        MongoIterable <String> collections = test.listCollectionNames();
        for (String collection: collections) {
            validate(db, test, collection, errors);
        }
    }
    
    /**
     * Validates JSON document over production and temporal databases
     * 
     * @param db
     * @param test
     * @param json 
     */
    private void validate(final MongoDatabase db, 
                          final MongoDatabase test, 
                          final String collection, 
                          final List<ValidationError> errors) {
        
        JsonSchemaLocator locator;
        JsonSchema schema;
        try {
            locator = getJsonSchemaLocator(collection);
            if (locator == null) {
                errors.add(new ValidationError("no json schema for " + collection + " found."));
                return;
            }
            schema = reader.read(locator);
        } catch (JsonSchemaException | URISyntaxException | MalformedURLException ex) {
            errors.add(new ValidationError("error reading schema for " + collection + " : " + ex.getMessage()));
            return;
        }
        
        MongoCollection<Document> colls = test.getCollection(collection);
        FindIterable<Document> docs = colls.find();
        for (Document doc : docs) {
            JsonObject jsonObject = Json.createReader(new StringReader(doc.toJson())).read().asJsonObject();
            
            final String id = jsonObject.getString("_id");
            
            List<ValidationError> erz = new ArrayList<>();
            schema.validate(jsonObject, erz, (PrimitiveSchema model, String pointer, JsonValue value, JsonValue parent, List<ValidationError> err) -> {
            
                if (db != test && model instanceof JsonObjectSchema && 
                    "/".equals(model.getJsonPointer())) { // look only in ROOT schema(s)

                    JsonObject object = locator.getSchemas(model.getId()).get(model.getJsonPointer());

                    JsonValue pkValue = object.get("primary_key");
                    if (pkValue != null) {
                        if (JsonValue.ValueType.ARRAY == pkValue.getValueType()) {
                            final Map<String, String> pkMap = new HashMap<>();
                            final JsonArray pkeys = pkValue.asJsonArray();
                            for (int i = 0, n = pkeys.size(); i < n; i++) {
                                JsonValue pkey = pkeys.get(i);
                                if (JsonValue.ValueType.STRING == pkey.getValueType()) {
                                    final String property = ((JsonString)pkey).getString();
                                    final JsonValue keyValue = value.asJsonObject().get(property);
                                    if (JsonValue.ValueType.STRING == keyValue.getValueType()) {
                                        pkMap.put(property, ((JsonString)keyValue).getString());
                                    }
                                }
                            }
                            if (!validatePKs(db, pkMap, value.asJsonObject())) {
                                err.add(new ValidationError(model.getId(), model.getJsonPointer(), "PK constraint violation: '" + pkMap + "'"));
                            }
                        }

                    }
                } else if (model instanceof JsonStringSchema &&
                    JsonValue.ValueType.STRING == value.getValueType()) {
                    JsonObject object = locator.getSchemas(model.getId()).get(model.getJsonPointer());
                    JsonValue fkValue = object.get("foreign_keys");
                    if (fkValue != null && JsonValue.ValueType.ARRAY == fkValue.getValueType() && 
                        !validateFKs(db, test, ((JsonString)value).getString(), fkValue.asJsonArray())) {
                        err.add(new ValidationError(model.getId(), model.getJsonPointer(), " FK constraint violation: '" + 
                                ((JsonString)value).getString() + "'"));
                    }
                }
            });
            
            for (ValidationError err : erz) {
                errors.add(new ValidationError(err.id, err.pointer, collection + "[" + id + "] " + err.pointer + " " + err.message));
            }
        }

    }

    public boolean validatePKs(MongoDatabase db, Map<String, String> values, JsonObject object) {
        final JsonString _schema = object.getJsonString("_schema");
        final String[] path = _schema.getString().split("/");
        final String collection = path[path.length - 1];
        
        for(String primary_key : values.values()) {
            if (hasId(db, primary_key, collection)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean validateFKs(MongoDatabase db, MongoDatabase test, String value, JsonArray fkeys) {
        for (int i = 0, n = fkeys.size(); i < n; i++) {
            JsonValue fk_schema = fkeys.get(i);
            if (JsonValue.ValueType.OBJECT == fk_schema.getValueType()) {
                if (!validateFK(db, test, value, fk_schema.asJsonObject())) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateFK(MongoDatabase db, MongoDatabase test, String value, JsonObject fk_schema) {

        final JsonValue _id = fk_schema.get("schema_id");
        if (_id == null) {
            return false;
        }
        if (JsonValue.ValueType.STRING != _id.getValueType()) {
            return false;
        }
        
        return hasId(db, ((JsonString)_id).getString(), value) || 
               (db != test && hasId(test, ((JsonString)_id).getString(), value));
    }
    
    private boolean hasId(MongoDatabase db, String collection, String id) {
        MongoCollection<Document> col = db.getCollection(collection);
        return col != null && col.countDocuments(Filters.eq("_id", id)) > 0;
    }

    /**
     * Returns JsonSchemaLocator for the given mongodb collection
     * 
     * @param collection the name of mongodb collection
     * @return JsonSchemaLocator which locate Json schema document
     * 
     * @throws URISyntaxException 
     */
    private JsonSchemaLocator getJsonSchemaLocator(final String collection) 
            throws URISyntaxException , MalformedURLException {
        
        JsonSchemaLocator locator = locators.get(collection);
        if (locator == null) {
            //final URL url = FKValidator.class.getClassLoader().getResource(collection + ".json");
            StringBuilder file = new StringBuilder(collection);
            file.setCharAt(0, Character.toLowerCase(collection.charAt(0)));
            file.append(".json");

            final URL url = GitHubSchemaLocator.GITHUB_SCHEMA_LOCATION.resolve(file.toString()).toURL();
            if (url != null) {
                locators.put(collection, locator = new GitHubSchemaLocator(url.toURI()));
            }
        }
        return locator;
    }
    
    public static void main(String[] args) throws URISyntaxException {
        Map<String, List<String>> params = parameters(args);
        
        if (params.isEmpty()) {
            System.out.println(HELP);
            System.exit(0);
        }

        final List<String> ldatabase = params.get("-database");
        if (ldatabase.isEmpty()) {
            System.out.println("missed 'database' parameter");
            System.exit(1);
        }
        final String database = ldatabase.get(0);

        final List<String> lhost = params.get("-host");
        final String host = lhost == null || lhost.isEmpty() ? "localhost" : lhost.get(0);
        
        final List<String> lport = params.get("-port");
        final Integer port = lport == null || lport.isEmpty() ? 27017 : Integer.parseInt(lport.get(0));
        
        final List<String> luser = params.get("-u");
        final String user = luser == null || luser.isEmpty() ? null : luser.get(0);
        
        final List<String> lpassword = params.get("-p");
        final String password = lpassword == null || lpassword.isEmpty() ? null : lpassword.get(0);

        final List<String> lprovisional = params.get("-provisional");
        final String provisional = lprovisional == null || lprovisional.isEmpty() ? database : lprovisional.get(0);

        //final MongoClient client = new MongoClient(host, port);
        final URI uri = new URI("mongodb", 
                        user != null ? (user + (password != null ? ":" + password : "")) : null, host, port, null, null, null);
        
        final MongoClient client = new MongoClient(new MongoClientURI(uri.toString()));
        
        final List<ValidationError> errors = new ArrayList<>();
        final FKValidator validator = new FKValidator(client, database);
        
        validator.validate(provisional, errors);
        
        for (ValidationError error : errors) {
            System.out.println(error.message);
        }
    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-host":
                case "-port":
                case "-u":
                case "-p":
                case "-database":
                case "-provisional":  values = parameters.get(arg);
                                      if (values == null) {
                                          values = new ArrayList(); 
                                          parameters.put(arg, values);
                                      }
                                      break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }
}

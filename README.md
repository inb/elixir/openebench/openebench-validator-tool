###  OpenEBench Database Validation Tool

Tool for consistency validation of the OpenEBench database.

###### Apache Maven build system
Build process uses [Apache Maven](https://maven.apache.org/) build system.

To build OpenEBench Database Validation Tool application:

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/elixir/openebench/openebench-validator-tool.git
>cd openebench-validator-tool
>mvn install
```

###### Tool execution
validator [-host, -port, -u, -p] -database [-provisional]  

parameters:  

-host              - mongodb host (default localhost)  
-port              - mongodb port (default 27017)  
-u                 - mongodb user  
-p                 - mongodb password  
-database          - mongodb database to validate  
-provisional       - mongodb additional (provisional) database to validate  

```shell
>java -jar openebench-validator-tool.jar -u user -p password -database openebench
```
